#!/bin/bash
source ../Secrets/vyos.creds
echo "***Backing up VYOS devices***"
ansible-playbook vyos.backup.yml
